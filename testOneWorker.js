let sortingTest = function(numbers) {
    if (!Array.isArray(numbers) || numbers.length !== 11) {
        console.log('Must provide an array as function parameter and the length should be 11')
        return;
    }
    this.numbers = numbers;
};

sortingTest.prototype.sort = function() {
    if (this.numbers === undefined) {
        return;
    }

    let swap;
    if (this.numbers[0] > this.numbers[5]) { swap = this.numbers[0]; this.numbers[0] = this.numbers[5]; this.numbers[5] = swap; }
    if (this.numbers[1] > this.numbers[6]) { swap = this.numbers[1]; this.numbers[1] = this.numbers[6]; this.numbers[6] = swap; }
    if (this.numbers[2] > this.numbers[7]) { swap = this.numbers[2]; this.numbers[2] = this.numbers[7]; this.numbers[7] = swap; }
    if (this.numbers[3] > this.numbers[8]) { swap = this.numbers[3]; this.numbers[3] = this.numbers[8]; this.numbers[8] = swap; }
    if (this.numbers[4] > this.numbers[9]) { swap = this.numbers[4]; this.numbers[4] = this.numbers[9]; this.numbers[9] = swap; }
    if (this.numbers[4] > this.numbers[10]) { swap = this.numbers[4]; this.numbers[4] = this.numbers[10]; this.numbers[10] = swap; }
    if (this.numbers[0] > this.numbers[3]) { swap = this.numbers[0]; this.numbers[0] = this.numbers[3]; this.numbers[3] = swap; }
    if (this.numbers[5] > this.numbers[8]) { swap = this.numbers[5]; this.numbers[5] = this.numbers[8]; this.numbers[8] = swap; }
    if (this.numbers[1] > this.numbers[4]) { swap = this.numbers[1]; this.numbers[1] = this.numbers[4]; this.numbers[4] = swap; }
    if (this.numbers[6] > this.numbers[9]) { swap = this.numbers[6]; this.numbers[6] = this.numbers[9]; this.numbers[9] = swap; }
    if (this.numbers[6] > this.numbers[10]) { swap = this.numbers[6]; this.numbers[6] = this.numbers[10]; this.numbers[10] = swap; }
    if (this.numbers[0] > this.numbers[2]) { swap = this.numbers[0]; this.numbers[0] = this.numbers[2]; this.numbers[2] = swap; }
    if (this.numbers[3] > this.numbers[6]) { swap = this.numbers[3]; this.numbers[3] = this.numbers[6]; this.numbers[6] = swap; }
    if (this.numbers[7] > this.numbers[9]) { swap = this.numbers[7]; this.numbers[7] = this.numbers[9]; this.numbers[9] = swap; }
    if (this.numbers[7] > this.numbers[10]) { swap = this.numbers[7]; this.numbers[7] = this.numbers[10]; this.numbers[10] = swap; }
    if (this.numbers[0] > this.numbers[1]) { swap = this.numbers[0]; this.numbers[0] = this.numbers[1]; this.numbers[1] = swap; }
    if (this.numbers[2] > this.numbers[4]) { swap = this.numbers[2]; this.numbers[2] = this.numbers[4]; this.numbers[4] = swap; }
    if (this.numbers[5] > this.numbers[7]) { swap = this.numbers[5]; this.numbers[5] = this.numbers[7]; this.numbers[7] = swap; }
    if (this.numbers[8] > this.numbers[9]) { swap = this.numbers[8]; this.numbers[8] = this.numbers[9]; this.numbers[9] = swap; }
    if (this.numbers[8] > this.numbers[10]) { swap = this.numbers[8]; this.numbers[8] = this.numbers[10]; this.numbers[10] = swap; }
    if (this.numbers[9] > this.numbers[10]) { swap = this.numbers[9]; this.numbers[9] = this.numbers[10]; this.numbers[10] = swap; }
    if (this.numbers[1] > this.numbers[2]) { swap = this.numbers[1]; this.numbers[1] = this.numbers[2]; this.numbers[2] = swap; }
    if (this.numbers[3] > this.numbers[5]) { swap = this.numbers[3]; this.numbers[3] = this.numbers[5]; this.numbers[5] = swap; }
    if (this.numbers[4] > this.numbers[6]) { swap = this.numbers[4]; this.numbers[4] = this.numbers[6]; this.numbers[6] = swap; }
    if (this.numbers[7] > this.numbers[8]) { swap = this.numbers[7]; this.numbers[7] = this.numbers[8]; this.numbers[8] = swap; }
    if (this.numbers[1] > this.numbers[3]) { swap = this.numbers[1]; this.numbers[1] = this.numbers[3]; this.numbers[3] = swap; }
    if (this.numbers[4] > this.numbers[7]) { swap = this.numbers[4]; this.numbers[4] = this.numbers[7]; this.numbers[7] = swap; }
    if (this.numbers[2] > this.numbers[5]) { swap = this.numbers[2]; this.numbers[2] = this.numbers[5]; this.numbers[5] = swap; }
    if (this.numbers[6] > this.numbers[8]) { swap = this.numbers[6]; this.numbers[6] = this.numbers[8]; this.numbers[8] = swap; }
    if (this.numbers[2] > this.numbers[3]) { swap = this.numbers[2]; this.numbers[2] = this.numbers[3]; this.numbers[3] = swap; }
    if (this.numbers[4] > this.numbers[5]) { swap = this.numbers[4]; this.numbers[4] = this.numbers[5]; this.numbers[5] = swap; }
    if (this.numbers[6] > this.numbers[7]) { swap = this.numbers[6]; this.numbers[6] = this.numbers[7]; this.numbers[7] = swap; }
    if (this.numbers[3] > this.numbers[4]) { swap = this.numbers[3]; this.numbers[3] = this.numbers[4]; this.numbers[4] = swap; }
    if (this.numbers[5] > this.numbers[6]) { swap = this.numbers[5]; this.numbers[5] = this.numbers[6]; this.numbers[6] = swap; }

    return this.numbers;
};

function testOneWorker() {
    console.log('Test one started');
    let sortingTestInstance = new sortingTest([88, 10, 20, 45, 29, 1, 9, 8, 4, 85, 44]);
    let begin = Date.now();

    // console.time('Sort calc time');
    // this test is currently for 1 billion records, if we want 10 bil. then we need to multiply 1e9 * 10
    let tenBillion = 1e9;
    let result = [];
    
    let index = 0;
    for (index; index < tenBillion; index++) {
        result = sortingTestInstance.sort();
    }
    // second way of messuring the code execution time.
    // console.timeEnd('Sort calc time');

    let end = Date.now();
    let timeSpent = (end - begin) / 1000 + "secs";
    
    postMessage({status: "done", time: timeSpent, resultArray: result});
}

testOneWorker();