let testArray = [];

let sortingTest = function(numbers) {
    
    if (!Array.isArray(numbers) || numbers.length !== 10000) {
        console.log('Must provide an array as function parameter and the length should be 11')
        return;
    }
    this.numbers = numbers;
};

sortingTest.prototype.sort = function(sortArray) {
    if (this.numbers !== undefined) {
        let length = sortArray.length;
        
        let i = 1;
        for (i; i < length; i++) {
            let key = sortArray[i];
            let j = i - 1;
            while (j >= 0 && sortArray[j] > key) {
                sortArray[j + 1] = sortArray[j];
                j = j - 1;
            }
            sortArray[j + 1] = key;
        }

        return sortArray;
    }

};

function testTwoWorker() {
    console.log('Test two started');

    let begin = Date.now();

    let sortArray = [];
    let a = 0;
    let b = 0;
    let index = 0;
    for (index; index < 10000; index++) {
        a = getRandomInt();
        b = getRandomInt();
        let number = a^b;
        sortArray.push(number);
    }
    
    let sortingTestInstance = new sortingTest(sortArray);
    let result = sortingTestInstance.sort(sortArray);    

    let end = Date.now();
    let timeSpent = (end - begin) / 1000 + "secs";
    
    postMessage({status: "done", time: timeSpent, resultArray: result});
}

function getRandomInt() {
    min = Math.ceil(100);
    max = Math.floor(10000);

    return Math.floor(Math.random() * (max - min + 1)) + min;
}

testTwoWorker();