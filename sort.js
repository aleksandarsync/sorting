let w = undefined;
let processing = false;

function testOne() {
  if (processing) {
    console.log('still processing the result');
    return;
  } else {
    w = new Worker("testOneWorker.js");
    document.getElementById("resultTestOne").innerHTML = "";
    processing = true;
  }
  
  w.onmessage = function(event) {
    
    if (event.data.status === "done") {
      processing = false;
      console.log("processing finished : ", event.data.resultArray);
    }
    document.getElementById("resultTestOne").innerHTML = 'Result for test one : ' + event.data.time;
  };
}

function testTwo() {
  if (processing) {
    console.log('still processing the result');
    return;
  } else {
    w = new Worker("testTwoWorker.js");
    document.getElementById("resultTestTwo").innerHTML = "";
    processing = true;
  }
  
  w.onmessage = function(event) {
    
    if (event.data.status === "done") {
      processing = false;
      console.log("processing finished : ", event.data.resultArray);
    }
    document.getElementById("resultTestOne").innerHTML = 'Result for test two : ' + event.data.time;
  };
}

function stopWorker() {
    if (w !== undefined && processing) {
        w.terminate();
        w = undefined;
        processing = false;
        console.log("processing stopped.")
    }
}
